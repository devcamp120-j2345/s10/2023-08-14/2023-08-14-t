public class App {
    public static void main(String[] args) throws Exception {
        int[] myArray = new int[]{0, 1, 2, 3};

        int[] myArray2 = { 2, 3,5,7,3};

        //System.out.println("Hello, World! Devcamp:" + App.sumNumbersV1());
        //System.out.println("Total number of array1 is: " + App.calculate(myArray));
        //System.out.println("Total number of array2 is: " + App.calculate(myArray2));
        //System.out.println("" + App.printHello(20));
        //System.out.println("" + App.printHello(21));

        App.printHello2(12);
        App.printHello2(13);
    }
    public static int sumNumbersV1(){
        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            sum+=i;
        }
        return sum;
    }
    public static int sumNumbersV1_1(){
        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            sum = sum + i;
        }
        return sum;
    }
    public static int calculate(int[] numbers){
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum+=numbers[i];
        }
        return sum;
    }
    public static String printHello(int num){
        if(num%2==0){
            return "Đây là số chẵn";
        }else{
            return "Đây là số lẻ";
        }
    }
    public static void printHello2(int num){
        if(num%2==0){
            System.out.println("Đây là số chẵn");
        }else{
            System.out.println("Đây là số lẻ");
        }
    }
}
